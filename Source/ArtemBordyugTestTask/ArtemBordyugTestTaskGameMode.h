// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ArtemBordyugTestTaskGameMode.generated.h"

UCLASS(minimalapi)
class AArtemBordyugTestTaskGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AArtemBordyugTestTaskGameMode();
};



