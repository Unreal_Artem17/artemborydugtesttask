// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "ArtemBordyugTestTask/Interfaces/Interaction.h"
#include "GameFramework/Actor.h"
#include "CPPBaseLamp.generated.h"

UCLASS()
class ARTEMBORDYUGTESTTASK_API ACPPBaseLamp : public AActor, public IInteraction
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPPBaseLamp();

protected:
	/** Point Light Component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UPointLightComponent* PointLight;

	/** Speed of the Light Switching */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float ChangeColorRate = 1.f;

	/** Current Color Light */
	UPROPERTY(ReplicatedUsing = OnRep_ColorLight)
		FLinearColor ColorLight;
	
	//~~~Begin IInteraction interface
	virtual void ActorsInteraction_Implementation(bool bIsActive) override;
	//~~~End IInteraction interface

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
private:
	FTimerHandle SwitchLampColorTimer;

	/** Change Lamp Visibility */
	virtual void SwitchStateLamp(bool bVisibility);

	/** Change Color Timer */
	virtual void StartStopTimer(bool bActive);
	FLinearColor GetRandomLightColor() const;

	UFUNCTION()
		void OnRep_ColorLight();
};
