// Fill out your copyright notice in the Description page of Project Settings.


#include "CPPBasePlate.h"
#include "ArtemBordyugTestTask/Interfaces/Interaction.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
ACPPBasePlate::ACPPBasePlate()
{
	USceneComponent* SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComp")); // Create Root Scene Component

	RootComponent = SceneComponent; // Setup Root Component

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxTrigger")); // Create Trigger Box
	TriggerBox->SetupAttachment(GetRootComponent()); // Attaching Trigger Box to Root Component
	TriggerBox->OnComponentBeginOverlap.AddDynamic(this, &ACPPBasePlate::OnBeginOverlap); // Trigger Binding OnComponentBeginOverlap
	TriggerBox->OnComponentEndOverlap.AddDynamic(this, &ACPPBasePlate::OnEndOverlap); // Trigger Binding OnComponentEndOverlap

	PlateMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlateMesh")); // Create PlateMesh
	PlateMesh->SetupAttachment(TriggerBox); // Attaching PlateMesh to Trigger Box

	TL_PlateMove = CreateDefaultSubobject<UTimelineComponent>(TEXT("PlateMove")); // Create Timeline 
	InterpFunction.BindUFunction(this, FName("TimelineFloatReturn")); // Bind to Return Float
}

void ACPPBasePlate::BeginPlay()
{
	Super::BeginPlay();
	TL_PlateMove->AddInterpFloat(Alpha, InterpFunction, FName("Erp"));
	TL_PlateMove->SetLooping(false);
	TL_PlateMove->SetIgnoreTimeDilation(true);
	PlateStartPosition = PlateMesh->GetRelativeLocation().Z;
}

// Calling on BeginOverlap 
void ACPPBasePlate::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(Cast<APawn>(OtherActor))
	{ 
		for (auto Lamp : LampActors)
		{
			if (Lamp && Lamp->GetClass()->ImplementsInterface(UInteraction::StaticClass()))
			{
				IInteraction::Execute_ActorsInteraction(Lamp, true);
			}
		}

		if (TL_PlateMove)
		{
			TL_PlateMove->Play();
		}
	}
}

//Calling on EndOverlap
void ACPPBasePlate::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	for (auto Lamp : LampActors)
	{
		if (Lamp && Lamp->GetClass()->ImplementsInterface(UInteraction::StaticClass()))
		{
			IInteraction::Execute_ActorsInteraction(Lamp, false);
		}
	}

	if (TL_PlateMove)
	{
		TL_PlateMove->Reverse();
	}
}

// Timeline
void ACPPBasePlate::TimelineFloatReturn(float Value)
{
	float DefaultRelativeZ = PlateMesh->GetClass()->GetDefaultObject<UStaticMeshComponent>()->GetRelativeLocation().Z;
	float ZLocation = FMath::Lerp(PlateStartPosition, PlateShift, Value);
	PlateMesh->SetRelativeLocation(FVector(PlateMesh->GetRelativeLocation().X, PlateMesh->GetRelativeLocation().Y, ZLocation));
}