// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPBaseLamp.h"
#include "Components/PointLightComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ACPPBaseLamp::ACPPBaseLamp()
{
 	// Set this Actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	PointLight = CreateDefaultSubobject<UPointLightComponent>(TEXT("PointLight")); // Create PointLight
	PointLight->SetupAttachment(GetRootComponent()); // PointLight Attaching to Root

	bReplicates = true; // Allow to Replicate Actor Class
}

// Function to On/Off the Light
void ACPPBaseLamp::ActorsInteraction_Implementation(bool bIsActive) 
{
	SwitchStateLamp(bIsActive);
	StartStopTimer(bIsActive);
}

// Change Lamp Visibility 
void ACPPBaseLamp::SwitchStateLamp(bool bVisibility)
{
	if(PointLight)
	{
		PointLight->SetVisibility(bVisibility);
	}
}

// Change Color Timer
void ACPPBaseLamp::StartStopTimer(bool bActive)
{
	if (HasAuthority())
	{
		if (bActive)
		{
			FTimerDelegate TimerDelegate;
			TimerDelegate.BindLambda([&]
				{
					ColorLight = GetRandomLightColor();
					OnRep_ColorLight();
				});
			GetWorldTimerManager().SetTimer(SwitchLampColorTimer, TimerDelegate, ChangeColorRate, true);
		}
		else
		{
			GetWorldTimerManager().ClearTimer(SwitchLampColorTimer);
		}
	}

}

// Random Color Values
FLinearColor ACPPBaseLamp::GetRandomLightColor() const
{
	const float RRand = FMath::RandRange(0.f, 1.f);
	const float GRand = FMath::RandRange(0.f, 1.f);
	const float BRand = FMath::RandRange(0.f, 1.f);
	
	return FLinearColor(RRand, GRand, BRand);
}

// Calls when Color Light Changes on the Server
void ACPPBaseLamp::OnRep_ColorLight()
{
	PointLight->SetLightColor(ColorLight);	
}

// Variable Replication Setup
void ACPPBaseLamp::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ACPPBaseLamp, ColorLight);
}