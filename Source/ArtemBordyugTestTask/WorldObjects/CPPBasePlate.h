// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/TimelineComponent.h"
#include "GameFramework/Actor.h"
#include "CPPBasePlate.generated.h"

UCLASS()
class ARTEMBORDYUGTESTTASK_API ACPPBasePlate : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPPBasePlate();

protected:
	/** TriggerBox Component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UBoxComponent* TriggerBox;

	/** PlateMesh Component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* PlateMesh;

	/** Timeline Component */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly)
		class UTimelineComponent* TL_PlateMove;

	/** Array Contains Lamp Actors */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Plate|Actors")
		TArray<AActor*> LampActors;

	/** Curve for Timeline */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Plate|Alpha")
		UCurveFloat* Alpha;

	/** Plate Shift Value */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float PlateShift = -20.f;

	/** Calling on BeginOverlap */
	UFUNCTION()
		void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/** Calling on EndOverlap */
	UFUNCTION()
		void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	/** Plate Start Position */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float PlateStartPosition;
	
	virtual void BeginPlay() override;

private:
	/** Timeline Return */
	UFUNCTION()
		void TimelineFloatReturn(float Value);
	FOnTimelineFloat InterpFunction;
};
